# bemote

bemote is a utility application that lets you control your computer volume from
your phone through a wifi connection.

## latest version
[**Click here to go to the latest release**](https://gitlab.com/bemote/bemote/tags/0.0.1)

### bemote status

bemote has been abandoned from some time, and now it is in maintenance mode, I
(boterock) am currently picking up the codebase and arranging the build system
and website.

Once that is arranged, the next goal is bringing support for android. That may
change some of the underlying structure or take some time.

The goal in the end is to make bemote available to android/ios users, and be
able to connect to win/mac/linux and androidtv, but that is a longer term goal.

If you are interested in following the project, please follow bemote on
twitter: [https://twitter.com/bemoteapp](https://twitter.com/bemoteapp)

If you have any issue with the app, you can write on twitter or create an issue
in this repository. Contact by e-mail will be fixed soon.